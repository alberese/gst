
from math import sqrt, exp

import numpy as np

a = np.array(
	[[4.75798344e+06, 0.00000000e+00, 1.00000000e+00],
	 [4.77184089e+06, 0.00000000e+00, 1.00000000e+00],
	 [4.77184089e+06, 1.00000000e+03, 1.00000000e+00]])

det_a = np.linalg.det(a)
sign, log = np.linalg.slogdet(a)
print(f"{sign}, {log}, {np.exp(log)}")
print(f"Det a: {det_a}")

b = np.array(
    [[-3.17056404e+05, -0.00000000e+00, -1.00000000e+00],
     [-3.36834766e+05, -0.00000000e+00, -1.00000000e+00],
     [-3.36834766e+05, -1.00000000e+03, -1.00000000e+00]]
)

det_b = np.linalg.det(b)
print(f"Det b: {det_b}")

c = np.array(
    [[3.17056404e+05, 4.75798344e+06, 1.00000000e+00],
     [3.36834766e+05, 4.77184089e+06, 1.00000000e+00],
     [3.36834766e+05, 4.77184089e+06, 1.00000000e+00]]
)

det_c = np.linalg.det(c)
print(f"Det c: {det_c}")

d = np.array(
    [[-3.17056404e+05, -4.75798344e+06, -0.00000000e+00],
     [-3.36834766e+05, -4.77184089e+06, -0.00000000e+00],
     [-3.36834766e+05, -4.77184089e+06, -1.00000000e+03]]
)

det_d = np.linalg.det(d)
print(f"Det d: {det_d}")

norm = sqrt(det_a*det_a + det_b*det_b + det_c*det_c)

print(f"Norm: {norm}")

a = det_a / norm
b = det_b / norm
c = det_c / norm
d = det_d / norm

print(f"a: {a}")
print(f"b: {b}")
print(f"c: {c}")
print(f"d: {d}")

