{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic spatial data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### *March 2018, June 2019. Mauro Alberti.*\n",
    "\n",
    "*Last version: 2024-08-15*\n",
    "\n",
    "*Last running version: 2024-08-15*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Developement code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**gst** is a library for the processing of geometric and geographic data, with a focus on structural geology data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we will plot geometric data into stereonets, prior to any other operation, we import *mplstereonet* and run the IPython command *%matplotlib inline*, that allows to incorporate Matplotlib plots in the Jupyter notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We import all classes/methods from the geometry sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gst.core.geometries.points import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Basic spatial data types: points and planes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reference axis orientations used in *gst* are the *x* axis parallel to East, *y* parallel to North and *z* vertical, upward-directed. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Cartesian points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A point is created by providing three Cartesian coordinates:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "p1 = Point(1.0, 2.4, 0.2)  # definition of a Point instance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When considering two points, we can calculate their 3D distance as well as their horizontal, 2D distance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "p2 = Point(0.9, 4.2, 10.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10.45657687773585"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "p1.distance(p2)  # 3D distance between two points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Among other methods, we can:\n",
    " - translate the point position by providing three offset cartesian values (*x, y* and *z*) or directly via a vector;\n",
    " - check if two points are within a given range of each other;\n",
    " - convert a point to a vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Cartesian planes "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A Cartesian plane can be defined in a few different ways, with the simplest one by providing three points within the plane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gst.core.geometries.planes import *\n",
    "pl1 = CPlane3D.from_points(Point(0, 0, 0), Point(1, 0, 0), Point(0, 1, 0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CPlane3D(0.00000000, 0.00000000, 1.00000000, 0.00000000)\n"
     ]
    }
   ],
   "source": [
    "print(pl1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The four coefficient returned (a, b, c and d) define the Cartesian plane as in the equation: \n",
    "\n",
    "   *ax + by + cz = d*\n",
    "\n",
    "For the given example, the equation is satisfied for all x and y values provided z is zero. We are therefore considering a horizontal plane passing through the frame origin.\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The versor normal to a Cartesian plane is obtained by the method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "normal_versor, error = pl1.normal_versor()  # versor (unit vector) normal to the provided Cartesian plane"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Vect3D(0.0000, 0.0000, 1.0000)\n"
     ]
    }
   ],
   "source": [
    "print(error)\n",
    "print(normal_versor)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example the normal versor is vertical."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can calculate the intersection, expressed as a versor, between two Cartesian planes: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(Vect3D(0.0000, -1.0000, 0.0000), )\n"
     ]
    }
   ],
   "source": [
    "pl1, pl2 = CPlane3D(1, 0, 0, 0), CPlane3D(0, 0, 1, 0)\n",
    "inters_v = pl1.intersects_other(pl2)  # intersection versor between two Cartesian planes \n",
    "print(inters_v)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
