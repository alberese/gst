python3 -m xdoctest gst
python3 -m unittest discover -s ./tests/ -p "test_*.py"


python3 -m xdoctest gst.core.mathematics.vectors3d
python3 -m xdoctest gst.core.geometries.space2d.grids.grid
python3 -m xdoctest gst.core.geology.utils.faults
python3 -m xdoctest gst.core.orientations.utils
python3 -m xdoctest gst.core.orientations.orientations
python3 -m xdoctest gst.core.deformations.space3d.rotations

python3 -m xdoctest gst.core.mathematics.arrays

python3 -m xdoctest gst.core.geometries.points
python3 -m xdoctest gst.core.geometries.lines
python3 -m xdoctest gst.core.geometries.polygons
python3 -m xdoctest gst.core.geometries.operators.joins


python3 -m unittest test_plane_line_intersections.TestPlaneAxisIntersections.test_plane_axis_intersections
python3 -m unittest test_plane_line_intersections.TestPlaneAxisIntersections.test_axis_within_plane
python3 -m unittest test_plane_line_intersections.TestPlaneAxisIntersections.test_axis_parallel_plane

python3 -m unittest test_point_projections.TestPointProjections.test_point_projection_outside_plane
python3 -m unittest test_point_projections.TestPointProjections.test_point_projection_in_plane

python3 -m unittest test_plane_plane_intersections.TestCartesianPlanesIntersections.test_cartesian_planes_intersection
python3 -m unittest test_plane_plane_intersections.TestCartesianPlanesIntersections.test_coincident_planes_intersection
python3 -m unittest test_plane_plane_intersections.TestCartesianPlanesIntersections.test_parallel_planes_intersections
python3 -m unittest test_plane_plane_intersections.TestCartesianPlanesIntersectionsAsPoint.test_other_as_point

python3 -m unittest test_plane_attitude_projection.TestCartesianPlanesAttitudeProjection.test_cartesian_plane_attitude_projection

##########

python3 -m unittest planes.test_planes_general.TestCartesianPlanesGeneric.test_shift_plane
python3 -m unittest tests.deformations.rotations.test_rotate

python3 -m unittest tests.polygons.test_intersections.TestPolygonIntersections

python3 -m unittest tests.polygons.test_profilers_polygons.TestSegmentProfilerPolygonIntersections.test_intersection_butterfly_02

python3 -m unittest tests.polygons.test_point_in_polygon


